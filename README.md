# gpio4
Control gpio in python on Linux.

Improved version based on Sysfs, features same as RPi.GPIO and [gpio3](https://pypi.org/project/gpio3)

Support `RaspberryPi / OrangePi / BananaPi...`


# Installation
Install from PyPI is suggested.

```bash
pip install gpio4
```

Or install from source.

```bash
git clone git@github.com:hankso/gpio4.git
cd gpio4
python setup.py build && sudo python setup.py install
```


# Usage
Want something like RPi.GPIO?

```python
>>> import gpio4.GPIO as GPIO
>>> GPIO.setmode(GPIO.BCM)
>>> GPIO.setup([12, 13], GPIO.IN)
>>> GPIO.input([12, 13])
[0, 0]
>>> p = GPIO.PWN(12)                    # Pin 12 will be set to output first
>>> p.start(30)                         # duty cycle is 30%
...
>>> from gpio4.constants import BOARD_NANO_PI as BOARD
>>> GPIO.setmode(BOARD)
>>> GPIO.setup([6, 7, 9], GPIO.OUTPUT)
>>> GPIO.output([6, 7, 9], [GPIO.HIGH, GPIO.LOW, GPIO.HIGH])
>>> GPIO.add_event_detect(8, GPIO.RAISING, bouncetime=300)
```

or call functions as you are using Arduino?

```python
>>> from gpio4.arduino import *
>>> pinMode(13, OUTPUT)
>>> pinMode(12, INPUT_PULLUP)
>>> digitalWrite(13, HIGH)
>>> digitalWrite(13, digitalRead(12))
>>> shiftIn(dataPin=12, clockPin=13, bitOrder=MSBFIRST)
170
```

Try the most basic but fastest Sysfs class

```python
>>> import select
>>> from gpio4 import SysfsGPIO
>>> from gpio4.constants import BOARD_ORANGE_PI_PC
>>> pin_name = 6
>>> pin_num = BOARD_ORANGE_PI_PC[pin_name]
>>> pin = SysfsGPIO(pin_num)            # default path: /sys/class/gpio/gpio#n
>>> pin.export = True                   # register pin through sysfs
>>> pin.direction = 'out'               # set Input/Output like `pinMode`
>>> pin.value = 1                       # set High/Low like `digitalWrite`
>>> pin.active_low = 1                  # High means 0V, Low means 5V (TTL)
>>> print(pin.value)                    # get current gpio level
>>> pin.export = False                  # unregister from sysfs
```

We also add support for SysfsLED since v0.1.0

```python
>>> from gpio4 import SysfsLED
>>> led = SysfsLED('act')               # default path: /sys/class/leds/act
>>> led.brightness = led.max_brightness # turn on LED
>>> led.brightness = 0                  # turn off LED
>>> led.brightness = 0x80               # only works with PWM LED etc.
>>> led.trigger = 'timer'               # now led should blink
>>> led.delay_on = 1000                 # you can change blink speed in ms
>>> led.delay_off = 1000
```

And SysfsPWM since v0.1.3

```python
>>> from gpio4 import SysfsPWM
>>> pwm = SysfsPWM(0)                   # default path: /sys/class/pwm/pwmchip0/pwm0
>>> pwm.frequency = 1250                # set period to 1 / 1.25kHz = 800000 ns
>>> pwm.percentage = 10                 # set duty cycle to 10% = 80000 ns
>>> pwm.polarity = 'inversed'           # change polarity only when not enabled
>>> pwm.enable = True                   # turn on/off pwm output
```

If you have any question on usage, it is strongly recommended to directly read well commented source codes.

Also check [kernel doc of SysfsGPIO](https://www.kernel.org/doc/Documentation/gpio/sysfs.txt)
and [kernel doc of SysfsPWM](https://www.kernel.org/doc/Documentation/pwm.txt).
